/* global chrome replyRemoteValue listenRemoteValueRequest diffuseRemoteValue listenDiffuseValueRequest requestRemoteValue UI Config Util WS Broker Snowflake*/

// eslint-disable-next-line no-unused-vars
let log, dbg, snowflake, config, debug;

const isServiceWorker = typeof chrome.offscreen !== 'undefined';
if (isServiceWorker) {
  const dummyFunction = () => {};
  // Ensure that the service worker wakes up and actually creates
  // the offscreen document (below).
  // Otherwise, when the browser is launched, none of the extension's
  // code is run, until the user opens the popup
  // (which starts the service worker).
  // See https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake-webext/-/issues/104#note_3070566
  //
  // TODO read up on offscreen document lifetime and ensure
  // that this covers all cases.
  chrome.runtime.onStartup.addListener(dummyFunction);
  chrome.runtime.onInstalled.addListener(dummyFunction);
}

(function () {
  'use strict';

  debug = true;

  async function init() {
    if (typeof chrome.offscreen === 'undefined') {
      await initOffscreen();
    } else {
      await initServiceWorker();
    }
  }

  async function initServiceWorker() {
    async function onRemoteRequestValue(kind, name) {
      if (kind === "snowflake-preference") {
        let result = await chrome.storage.local.get([name]);
        replyRemoteValue(kind, name, result[name]);
      }
    }

    function onRemoteRequestValueWrapper(kind, name) {
      onRemoteRequestValue(kind, name);
    }

    listenRemoteValueRequest(onRemoteRequestValueWrapper);

    function onRemoteDiffuseValue(kind, name, value) {
      if (kind === "snowflake-preference") {
        let obj = {};
        obj[name] = value;
        chrome.storage.local.set(obj);
      }
    }

    listenDiffuseValueRequest(onRemoteDiffuseValue);


    chrome.runtime.onMessage.addListener(onMessage);

    const existingContexts = await chrome.runtime.getContexts({
      contextTypes: ['OFFSCREEN_DOCUMENT']
    });

    /* Service worker may get terminated, the offscreen document will outlive it in this case  */
    if (existingContexts.length > 0) {
      return;
    }

    await chrome.offscreen.createDocument({
      url: 'offscreen.html',
      reasons: [chrome.offscreen.Reason.WEB_RTC],
      justification: 'Use WebRTC.',
    });


    // Keep other components alive
    sendMessage('snowflake-serviceworker-init', {});

    let consented = await chrome.storage.local.get(["snowflake-consented"]);
    if (consented["snowflake-consented"] !== true) {
      chrome.tabs.create({url: chrome.runtime.getURL("consent.html")});
    }
  }

  class WebExtOffscreenUI extends UI {
    constructor() {
      super();
      this.running = false;
    }

    checkNAT() {
      Util.checkNATType(config.datachannelTimeout).then((type) => {
        console.log("Setting NAT type: " + type);
        this.natType = type;
      }).catch((e) => {
        console.log(e);
      });
    }

    initNATType() {
      this.natType = "unknown";
      this.checkNAT();
      setInterval(() => {
        this.checkNAT();
      }, config.natCheckInterval);
    }

    tryProbe() {
      WS.probeWebSocket(config.defaultRelayAddr)
      .then(
        () => {
          this.missingFeature = false;
          this.setEnabled(true);
        },
        () => {
          log('Could not connect to bridge.');
          this.missingFeature = 'popupBridgeUnreachable';
          this.setEnabled(false);
        }
      );
    }

    postActive() {
      super.postActive();
      diffuseRemoteValue("snowflake-offscreen-status", "clientCount", this.clients);
      diffuseRemoteValue("snowflake-offscreen-status", "clientTotal", this.getTotal());
    }

    getTotal() {
      return this.stats.reduce((t, c) => t + c, 0);
    }
  }

  async function sendMessage(kind, data) {
    await chrome.runtime.sendMessage({
      kind: kind,
      data: data
    });
  }

  // eslint-disable-next-line no-unused-vars
  async function onMessage(message, sender, reply) {
    console.log('Received message: ' + message.kind);
  }


  async function initOffscreen() {

    chrome.runtime.onMessage.addListener(onMessage);

    config = new Config("webext");

    let ui = new WebExtOffscreenUI();

    let broker = new Broker(config);

    snowflake = new Snowflake(config, ui, broker);

    log = function (msg) {
      console.log('Snowflake: ' + msg);
    };

    dbg = log;

    log('== snowflake proxy ==');

    dbg('Contacting Broker at ' + broker.url);

    sendMessage('snowflake-offscreen-init', {});

    function replyOffscreenStatus(kind, name) {
      if (kind === "snowflake-offscreen-status") {
        if (name === "clientCount") {
          replyRemoteValue(kind, name, ui.clients);
          return;
        }
        if (name === "running") {
          replyRemoteValue(kind, name, ui.running);
          return;
        }
        if (name === "clientTotal") {
          replyRemoteValue(kind, name, ui.getTotal());
        }
        replyRemoteValue(kind, name, "");
      }
    }

    listenRemoteValueRequest(replyOffscreenStatus);

    function diffuseValueReceiver(kind, name, value) {
      if (kind === "snowflake-preference") {
        if (name === "snowflake-enabled") {
          if (value === true) {
            ui.initNATType();
            snowflake.beginServingClients();
            ui.running = true;
            diffuseRemoteValue("snowflake-offscreen-status", "running", true);
          } else {
            snowflake.disable();
            ui.running = false;
            diffuseRemoteValue("snowflake-offscreen-status", "running", false);
          }
        }
      }
    }

    listenDiffuseValueRequest(diffuseValueReceiver);

    let shouldAutoStart = await requestRemoteValue("snowflake-preference", "snowflake-enabled");
    console.log("Should auto start: " + shouldAutoStart);
    if (shouldAutoStart === true) {
      ui.initNATType();
      snowflake.beginServingClients();
      ui.running = true;
      diffuseRemoteValue("snowflake-offscreen-status", "running", true);
    }
  }

  // Initialize the extension
  init();
}());